(function ($) {
  'use strict';
  $('.logoDecameron').prop('checked', false);
  $('.logo_white').prop('checked', false);
  $('.tamanios').prop('checked', true);
  $('.checkcta').prop('checked', false);
  $('.checkcolor').prop('checked', false);
  $('input[type=radio]').prop('checked', false);
  $('textarea').val('');

  /* Acomodar tamaño ventana */
  let fullHeight = function () {
    $('.js-fullheight').css('height', $(window).height());
    $(window).resize(function () {
      $('.js-fullheight').css('height', $(window).height());
    });
  };

  fullHeight();

  $('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active');
    $('#sidebar').css('max-height', `${$(window).height()}`);
  });

/*    
  $('input[name=logodeca]').change(function () {
    $('.logos').toggle();
  });
    
  $('input[name=logo_whi]').change(function () {
    $('.logow').toggle();
  });
*/  
    $('input[name=logo_whi]').click(function(){
    if ($(this).is(':checked'))
        {
            $('.logow').show();
            $('input[name=logodeca]').prop("checked", false);
            $('.logos').hide();
        }else
            {
                $('.logow').hide();
                $('.logos').show();
                $('input[name=logodeca]').prop("checked", true);
            }
    });
    
    $('input[name=logodeca]').click(function(){
        if ($(this).is(':checked'))
        {
            $('.logos').show();
            $('.logow').hide();
            $('input[name=logo_whi]').prop("checked", false);
        }else
            {
                $('.logos').hide();
                $('.logow').show();
                $('input[name=logo_whi]').prop("checked", true);
            }
    });
  
  $('input[name=tamanio]').change(function () {
    let valueIn = $(this).val();
    if ($(`.${valueIn}`).parent().children().length > 1) {
      let count = 0;
      $(`.${valueIn}`)
        .parent()
        .children()
        .each(function () {
          if ($(this).css('display') == 'none') {
            count++;
          }
        });
      if (count == 0 && $(`.${valueIn}`).css('display') != 'none') {
        $(`.${valueIn}`).toggle();
      } else if (count == 1 && $(`.${valueIn}`).css('display') != 'none') {
        $(`.${valueIn}`).toggle();
        $(`.${valueIn}`).parent().toggle();
      } else if (count == 1 && $(`.${valueIn}`).css('display') == 'none') {
        $(`.${valueIn}`).toggle();
      } else if (count == 2 && $(`.${valueIn}`).css('display') == 'none') {
        $(`.${valueIn}`).parent().toggle();
        $(`.${valueIn}`).toggle();
      }
    } else {
      $(`.${valueIn}`).parent().toggle();
    }
  });

  $('input[name=radio-image]').change(function () {
    if ($(this).val() == 'empty') {
      $('.dimensions').each(function () {
        let linearGrad = '';
        if (
          $(this)
            .css('background-image')
            .match(/\blinear-gradient\b/gi)
        ) {
          let allBack = $(this).css('background-image');
          linearGrad = allBack.split('url')[0];
        }
        $(this).removeAttr('style');
        $('input[name=radio-gama]').trigger('eventoPersonalizado');
      });
    } else {
      let src = $(this).siblings('img').attr('src');
      $('.dimensions').each(function () {
        let linearGrad = '';
        if (
          $(this)
            .css('background-image')
            .match(/\blinear-gradient\b/gi)
        ) {
          let allBack = $(this).css('background-image');
          linearGrad = allBack.split('url')[0];
        }
        $(this).css({
          // 'background-color':'rgba(255,255,255,0.7)',
          'background-image': `${linearGrad}url(${src})`,
          'background-size': 'cover',
          'background-repeat': 'no-repeat',
          'background-position': 'center 0',
          'background-color' : '#051A65',
          // 'opacity': '0.7',
          'position': 'relative',
          // 'filter': 'brightness(50%)',
        });
      });
    }
  });

  $('.btn-cta').click(function () {
    let btnSeleccionado = $(this);
    $('.dimensions').each(function () {
      let idDim = $(this).attr('id');
      if ($(`#${idDim} .btn-cta`).length >= 1) {
        $(`#${idDim} .btn-cta`).not(btnSeleccionado).remove();
      }
      btnSeleccionado.clone().appendTo($(this));
      $('#gif').show();
    });
  });

  $('input[name=radio-color]').change(function () {
    let color = $(this).siblings('.contentcolor').children('.colorbox').css('background-color');
    $('.dimensions').each(function () {
      $(this).children('.btn').css('border-color', color);
      $(this).children('.btn').css('background-color', 'transparent');
      $(this).children('.btn').css('color', color);
    });
    $('.btn-cta').each(function () {
      $(this).css('border-color', color);
      $(this).css('background-color', 'transparent');
      $(this).css('color', color);
    });
    
    $('.btn-cta').hover(
      function () {
        if(color == 'rgb(255, 255, 255)'){
          $(this).css('background-color', color);
          $(this).css('color', '#000');
        }else{
          $(this).css('background-color', color);
          $(this).css('color', '#fff');
        }        
      },
      function () {
        $(this).css('border-color', color);
        $(this).css('background-color', 'transparent');
        $(this).css('color', color);
      }
    );
  });

  /* COPY */
  $('input[name=radio-copy]').change('', function () {
    let content = '';
    let copiedText = '';
    if ($(this).val() != 5) {
      copiedText = $(this).siblings('label').text();
      $('textarea').prop('disabled', true);
    } else {
      $('textarea').prop('disabled', false);
      copiedText = $(this).siblings('textarea').val();
    }
    $('.dimensions').each(function () {
      let idDim = $(this).attr('id');
      content = $(`#${idDim} p`);
      content.text(copiedText);
    });
  });

  $('textarea').bind('keyup', function () {
    let customText = $(this).val();
    $('.dimensions p').each(function () {
      $(this).text('');
      $(this).text(customText);
    });
  });

  let gamas = [
    { fuente: 'rgb(0, 55, 125)', fondo: 'rgba(255,0,0,0)', sombra: '' },
    { fuente: 'rgb(255, 190, 0)', fondo: 'rgba(255,0,0,0)', sombra: ''},
    { fuente: 'rgb(255,255,255)', fondo: 'rgba(255,0,0,0)', sombra: '' },
    { fuente: 'rgb(0, 200, 255)', fondo: 'rgba(255,0,0,0)', sombra: ''},
    // { fuente: 'rgb(112,47,109)', fondo: 'rgba(255,0,0,0)', sombra: '0 0 0.2em #8F7'}, 
    // { fuente: 'rgb(0,0,0)', fondo: 'rgba(255,0,0,0)', sombra:  '0 0 0.2em #8F7'}, 
  ];

  $('input[name=radio-gama]').bind('change eventoPersonalizado', function () {
    let selected = false;
    let selectedGama = gamas[$(this).val()];
    $('input[name=radio-gama]').each(function () {
      if ($(this).is(':checked')) {
        selected = true;
        selectedGama = gamas[$(this).val()];
      }
    });
    if (selected) {
      $('.dimensions').each(function () {
        let imageUrl = '';
        if ($(this).css('background-image')) {
          imageUrl = `, ${$(this).css('background-image')}`;
        }
        if (
          $(this)
            .css('background-image')
            .match(/\blinear-gradient\b/gi)
        ) {
          let linear = $(this).css('background-image');
          let url = linear.split(' ')[8];
          imageUrl = `, ${url}`;
        }
        $(this).css({
          color: selectedGama.fuente,
          'text-shadow': selectedGama.sombra,
        });
      });
    }
  });

  $('#limpiar').click(function () {
    $('#gif').hide();
    $('input[type=radio]').prop('checked', false);
    $('.tamanios').prop('checked', true);
    $('.logoDecameron').prop('checked', false);
    $('.logo_white').prop('checked', false);
    $('.logos').css('display', 'none');
    $('.logow').css('display', 'none');
    $('.checkcta').prop('checked', false);
    $('.checkcolor').prop('checked', false);
    $('textarea').val('');
    $('.dimensions').removeAttr('style');
    $('.centerbox').removeAttr('style');
    $('.dimensions p').text('');
    $('.dimensions .btn').remove();
  });

  $('input[name=radio-cta]').change(function () {
    if ($(this).is(':checked')) {
      $('.dimensions .btn').toggle();
    } else if ($(this).is(':not(:checked)')) {
      $('.dimensions .btn').toggle();
    }
  });
})(jQuery);
